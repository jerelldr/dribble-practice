import { IntercapitolPage } from './app.po';

describe('intercapitol App', function() {
  let page: IntercapitolPage;

  beforeEach(() => {
    page = new IntercapitolPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
