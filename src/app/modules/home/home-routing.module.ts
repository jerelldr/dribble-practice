import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./view/home/home.component";
import { extract } from "app/core/";
import { Shell } from "app/shell/shell.service";

const routes: Routes = [
  Shell.childRoutes([
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", component: HomeComponent, data: { title: extract("Home") } }
  ])
];

@NgModule({
  imports: [[RouterModule.forChild(routes)]],
  exports: [RouterModule],
  providers: []
})
export class HomeRoutingModule {}
