import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CoreModule } from "app/core";
import { SharedModule } from "app/shared";
import { ShellModule } from "app/shell/shell.module";

import { HomeComponent } from "./view/home/home.component";
import { HomeRoutingModule } from "./home-routing.module";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,
    ShellModule,
    HomeRoutingModule
  ],
  declarations: [HomeComponent]
})
export class HomeModule {}
