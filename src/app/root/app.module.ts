import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./view/app.component";
import { AppRoutingModule } from "./app-routing.module";
import { SharedModule } from "app/shared";
import { CoreModule } from "app/core";
import { HomeModule } from "app/modules/home/home.module";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    CoreModule,
    SharedModule,
    HomeModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
