import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  title = "app works!";

  ngOnInit() {
    let docWidth = document.documentElement.offsetWidth;
    let docHeight = document.documentElement.offsetHeight;

    [].forEach.call(document.querySelectorAll("*"), function(el) {
      if (el.offsetWidth >= docWidth) {
        console.log(el);
      }
      if (el.offsetHeight >= docHeight) {
        console.log(el);
      }
    });
  }
}
