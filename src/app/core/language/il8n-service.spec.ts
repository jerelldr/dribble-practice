/* tslint:disable:no-unused-variable */

import { TestBed, inject } from "@angular/core/testing";
import { Il8nService } from "./il8n-service";

describe("Il8nService", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Il8nService]
    });
  });

  it("should ...", inject([Il8nService], (service: Il8nService) => {
    expect(service).toBeTruthy();
  }));
});
