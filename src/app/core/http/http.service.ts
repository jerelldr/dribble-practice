import { Injectable, Injector, Optional, Inject } from "@angular/core";
import { HttpClient, HttpHandler, HttpInterceptor } from "@angular/common/http";

@Injectable()
export class HttpService extends HttpClient {
  constructor(
    private httpHandler: HttpHandler,
    private injector: Injector,
    private interceptors: HttpInterceptor
  ) // @Optional()
  // @Inject(HTTP_DYNAMIC_INTERCEPTORS)
  {
    super(httpHandler);
  }
}
