import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(): boolean {
    if (!this.isProtectedRoute(this.router.url)) {
      console.log("not protected");
      return true;
    }
    console.log("protected");
    this.router.navigate(["/home"], { replaceUrl: true });
  }

  private isProtectedRoute(url: string): boolean {
    return url === "/protected";
  }
}
