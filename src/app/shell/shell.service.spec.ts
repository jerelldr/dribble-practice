/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from "@angular/core/testing";
import { Shell } from "./shell.service";

describe("Shell", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Shell]
    });
  });

  it("should ...", inject([Shell], (service: Shell) => {
    expect(service).toBeTruthy();
  }));
});
