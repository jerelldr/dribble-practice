import { Injectable } from "@angular/core";
import { ShellComponent } from "./view/shell.component";
import { Routes, Route } from "@angular/router";
import { AuthGuard } from "app/core/auth/auth.guard";

@Injectable()
export class Shell {
  static childRoutes(routes: Routes): Route {
    return {
      path: "",
      component: ShellComponent,
      children: routes,
      canActivate: [AuthGuard],
      data: { reuse: true }
    };
  }
  constructor() {}
}
