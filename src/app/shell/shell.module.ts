import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";

import { ShellComponent } from "./view/shell.component";
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';

@NgModule({
  imports: [CommonModule, RouterModule, BrowserModule],
  declarations: [ShellComponent, HeaderComponent, FooterComponent, SidebarComponent]
})
export class ShellModule {}
